use crate::config_parser;
use std::io::{BufRead, BufReader};
use std::sync::mpsc;
use std::thread;
use std::process;

const DEFAULT_INTERVAL: u64 = 1;
const DEFAULT_DO_SEPARATOR: bool = true;

#[derive(Debug)]
pub struct BlockOutput {
    pub block_id: usize,
    pub output: String,
    pub do_separator: bool,
}

pub fn spawn_block(
    block_id: usize,
    tx_channel: mpsc::Sender<BlockOutput>,
    block: &config_parser::Block,
) {
    if block.continuous.unwrap_or(false) {
        spawn_continuous_block(block_id, tx_channel, block.clone());
    } else {
        spawn_interval_block(block_id, tx_channel, block.clone());
    }
}

fn spawn_continuous_block(
    block_id: usize,
    tx_channel: mpsc::Sender<BlockOutput>,
    block: config_parser::Block,
) {
    use std::process::Stdio;
    let thread = {
        let mut thread_name = String::from("Continuous block: ");
        thread_name.push_str(block.command.as_str());
        thread::Builder::new()
            .name(thread_name)
    };
    thread.spawn(move || {
        let stdout = process::Command::new("sh")
            .arg("-c")
            .arg(block.command.clone())
            .stdout(Stdio::piped())
            .spawn()
            .unwrap()
            .stdout
            .unwrap();
        let stdout_reader = BufReader::new(stdout);
        stdout_reader
            .lines()
            .filter_map(|line| line.ok())
            .for_each(|line| {
                tx_channel
                    .send(BlockOutput {
                        block_id,
                        output: make_command_output(line, &block),
                        do_separator: block.do_separator.unwrap_or(DEFAULT_DO_SEPARATOR),
                    })
                    .unwrap();
            });
    }).unwrap();
}

fn spawn_interval_block(
    block_id: usize,
    tx_channel: mpsc::Sender<BlockOutput>,
    block: config_parser::Block,
) {
    let thread = {
        let mut thread_name = String::from("Interval block: ");
        thread_name.push_str(block.command.as_str());
        thread::Builder::new()
            .name(thread_name)
    };
    thread.spawn(move || block_with_interval(block_id, tx_channel, block)).unwrap();
}

fn block_with_interval(
    block_id: usize,
    tx_channel: mpsc::Sender<BlockOutput>,
    block: config_parser::Block,
) {
    use std::time::Duration;
    let mut new_command = process::Command::new("sh");
    new_command.arg("-c").arg(block.command.clone());
    loop {
        let command_output = String::from_utf8(new_command.output().unwrap().stdout).unwrap();
        tx_channel
            .send(BlockOutput {
                block_id,
                output: make_command_output(command_output, &block),
                do_separator: block.do_separator.unwrap_or(DEFAULT_DO_SEPARATOR),
            })
            .unwrap();
        if block.interval.unwrap_or(DEFAULT_INTERVAL) == 0 {
            return; // If the interval is 0, the command only runs once, so we'll just exit the thread
        }
        thread::sleep(Duration::from_secs(
            block.interval.unwrap_or(DEFAULT_INTERVAL) as u64,
        ));
    }
}

fn make_command_output(command_output: String, block: &config_parser::Block) -> String {
    let mut output = String::new();
    if block.align.is_some() {
        use crate::config_parser::Alignment;
        match block.align.unwrap() {
            Alignment::Left => output.push_str("%{l}"),
            Alignment::Right => output.push_str("%{r}"),
            Alignment::Center => output.push_str("%{c}"),
        }
    }

    // Everything pushed after this will be prepended to the command output
    output.push_str(block.label.clone().unwrap_or(String::new()).as_str());

    output.push_str(command_output.trim());
    // Everything pushed after this will be appended to the command output
    output.push_str("%{B-F-U-T-}%{-u}%{-o}"); // Reset each block
    output.trim().to_string()
}
