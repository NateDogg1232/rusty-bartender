pub mod block;
pub mod config_parser;
use clap::{App, Arg};

use std::sync::mpsc;

const DEFAULT_SEPARATOR: &str = " | ";

fn main() {
    let matches = App::new("Rusty Bartender")
        .version(clap::crate_version!())
        .author(clap::crate_authors!())
        .about(clap::crate_description!())
        .arg(Arg::with_name("config")
             .default_value("main.rbar")
             .value_name("FILE")
        ) .after_help("NOTE:\nAll config files must be pulled from $XDG_CONFIG_HOME/rusty-bartender/ or from ~/.config/rusty-bartender/")
        .get_matches();

    // Let's get our config file's name
    let config_file = {
        let mut config_home = get_config_home();
        let config_file = clap::value_t!(matches.value_of("config"), String).unwrap();
        config_home.push_str("/rusty-bartender/");
        config_home.push_str(config_file.as_str());
        config_home
    };

    let config: config_parser::Config = config_parser::load_and_parse_config(config_file.as_str());

    let separator: String = config.separator.unwrap_or(String::from(DEFAULT_SEPARATOR));

    let (block_tx, block_rx) = mpsc::channel(); // Make our channels

    // Read the config file and start the blocks.
    for (id, block) in config.block.iter().enumerate() {
        block::spawn_block(id, block_tx.clone(), block);
    }

    let mut block_map = std::collections::HashMap::new();
    loop {
        let block_output = block_rx.recv().unwrap();
        block_map.insert(block_output.block_id, block_output);
        // Update the output of the bar
        let mut block_vec = Vec::new();
        for x in block_map.iter() {
            block_vec.push(x);
        }
        // Sort the blocks
        block_vec.sort_by(|(a, _), (b, _)| a.partial_cmp(b).unwrap());
        //Output them one by one with a separator between
        for i in 0..block_vec.len() {
            let (_, block_output) = block_vec[i];
            if block_output.do_separator {
                print!("{}{}", block_output.output, separator);
            } else {
                print!("{}", block_output.output);
            }
        }
        println!();
        // We need to give time for lemonbar to process the output, so we sleep for 50ms
        std::thread::sleep(std::time::Duration::from_millis(50));
    }
}

fn get_config_home() -> String {
    // Let's try to get the XDG_CONFIG_HOME environment variable
    std::env::var("XDG_CONFIG_HOME").unwrap_or({
        // Couldn't get the XDG_CONFIG_HOME variable, so now we'll build it using $HOME
        let mut ret = std::env::var("HOME").unwrap_or_else(|_| {
            eprintln!("XDG_CONFIG_HOME nor HOME are set!");
            eprintln!("Please set either or both of these environment variables before running this program");
            std::process::exit(1)
        });
        ret.push_str("/.config");
        ret
    })
}
