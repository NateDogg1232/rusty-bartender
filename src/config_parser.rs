use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Config {
    pub separator: Option<String>,
    pub block: Vec<Block>,
}

#[derive(Deserialize, Debug, Copy, Clone)]
pub enum Alignment {
    Left,
    Right,
    Center,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Block {
    pub command: String,
    pub label: Option<String>,
    pub interval: Option<u64>,
    pub continuous: Option<bool>,
    pub align: Option<Alignment>,
    pub do_separator: Option<bool>,
}

pub fn load_and_parse_config(config_path: &str) -> Config {
    use std::io::BufRead;
    let config_file = match std::fs::File::open(config_path) {
        std::io::Result::Err(e) => {
            eprintln!("Could not open the config file {}: {}", config_path, e);
            std::process::exit(1);
        }
        std::io::Result::Ok(f) => f,
    };

    let mut config_string = String::new();
    let config_reader = std::io::BufReader::new(config_file);
    for line in config_reader.lines() {
        config_string.push_str(line.unwrap().as_str());
        config_string.push('\n');
    }
    match toml::from_str(config_string.as_str()) {
        Ok(r) => r,
        Err(e) => {
            eprintln!("ERROR: Could not parse config {}:\n{}\nExiting with exit code 1", config_path, e);
            std::process::exit(1);
        }, 
    }
}
