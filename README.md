# Rusty Bartender

A bartender for Lemonbar

[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)

## Usage:
```
USAGE:
    rusty-bartender [FILE]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <FILE>     [default: main.rbar]

NOTE:
All config files must be pulled from $XDG_CONFIG_HOME/rusty-bartender/ or from ~/.config/rusty-bartender/
```

## Installation:

Make sure you have cargo installed and configured. To do this, use [rustup.rs](https://rustup.rs). Then clone the repository, enter the directory and run `cargo install --path .` Once this is done, you should be able to run rusty-bartender.

## Configuration:

Configuration is done through a toml file in the directory `$XDG_CONFIG_HOME/rusty-bartender/` or `~/.config/rusty-bartender`. The config file MUST be placed into this directory. This is for the simplicity of the code.

### Example Configuration:

```toml
separator = " | "

[[block]]
command = "echo Bar 1!"

[[block]]
command = "$LOCAL/lib/i3blocks/battery BAT0" 
label = "BAT0: "
interval = 5

[[block]]
command = "$LOCAL/lib/i3blocks/battery BAT1"
label = "BAT1: "
interval = 5

[[block]]
command = "date"

[[block]
command = "xtitle -s"
continuous = true

```

### Configuration Options:

Configuration is split into two categories, top level, and blocks. The top level configuration is the configuration options that affect the entire bar. 

#### Top-level Configurations:

* separator: OPTIONAL The string that will be placed between every block

#### Block-level:

* command: REQUIRED The command that will be run. All commands are run using `/bin/sh`
* interval: OPTIONAL The interval in which the command will be run (in seconds). If set to 0, the command will only run once. The default is 1.
* continuous: OPTIONAL Whether the command is continuous or not. If this is set, interval is ignored.
* label: OPTIONAL This string is optional and will be appended to the beginning of each command.
* align: OPTIONAL This string can either be Left, Right, or Center and will change how this block and all blocks following it are aligned
* do_separator: OPTIONAL Set this to false to not do the separator at the end of this block

An example of a block configuration:
```toml
[[block]]
commmand = "date"
interval = 2
label = "Date: "
```

## Scripting:

In order for scripts to be usable in multiple block-based status bars (like i3blocks), I set the environment variable $RUSTYBAR to "rusty" so one can customize the output for rusty-bartender.

An example of use
```bash
#!/bin/sh
if [ -n "$RUSTYBAR" ]; then
    echo Using rusty-bartender!
fi

```

